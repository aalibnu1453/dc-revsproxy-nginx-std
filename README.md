command :
<br>&emsp;build and run container : $ docker-compose up -d
<br>&emsp;check list container : $ docker ps --format "table{{.ID}}\t{{.Names}}\t{{.Status}}\t{{.Ports}}"
<br>
<br>
check curl in terminal :
<br>&emsp;$ curl -I http://[your local ip]:5001
<br>
<br>
testing in browser :
<br>&emsp;address http://[your local ip]:5001
 
